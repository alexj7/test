import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailComponent } from './pages/Detail/detail.component';
import { ListComponent } from './pages/List/list.component';
import { TestComponent } from './pages/test/test.component';

const routes: Routes = [
  {
  component: TestComponent,
  path: 'logic',
  },
  {
  component: ListComponent,
  path: ''
  },
  {
  component: DetailComponent,
  path: 'detail/:id'
  },
  { path: '**', redirectTo: '/' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
