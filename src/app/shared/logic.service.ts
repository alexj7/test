import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LogicService {

  constructor() { }


  
  public getRange(a:number, b:number): any {

    let square:any = []

    for( let number = a; number <= b; number++ ){
      console.log(number)
      let dividers = []

      for (let aux = 1; aux <= number; aux++){
        if(number % aux == 0){
          dividers.push(aux)
        }
      }

      let sum_numbers = dividers.reduce((x,y) => x + (y * y), 0)

      if(Number.isInteger(Math.sqrt(sum_numbers))){
        square.push({
          number,
          'value': sum_numbers 
        })
      }

    }
    
    return new Observable((observer) => {
      return observer.next(
        {
          square
        });
    });
    }
}
