import { Injectable } from '@angular/core';
import { Observable, throwError, Subject } from "rxjs";
import { HttpClient, } from "@angular/common/http";
import { catchError } from "rxjs/internal/operators/catchError";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public url = 'https://reqres.in/api/users'

  constructor(public http: HttpClient) { }

  public getUser(page?:number): Observable<any> {
  return this.http.get(this.url + `?page=${page}`).pipe((response => response),
      catchError(this.handleError)
    );
  }

  public getSelectedUser(user_id?:number): Observable<any> {
  return this.http.get(this.url + `/${user_id}`).pipe((response => response),
      catchError(this.handleError)
    );
  }

  public updateUser(body:any): Observable<any> {
    return this.http.put(this.url + '/' + body.id, {body}).pipe((response => response),
    catchError(this.handleError)
  );
  }

  public createUser(body:any): Observable<any> {
    return this.http.post(this.url, {body}).pipe((response => response),
      catchError(this.handleError)
    );
  }

  public delUser(id:number): Observable<any> {
    return this.http.delete(this.url + '/' + id).pipe((response => response),
      catchError(this.handleError)
    );
  }






  private handleError(error: any): Observable<any> {
    let err = error;
      return throwError(error);
  }



  public address = [
    {
      location:    'Calle Miranda, Caracas 1060, Distrito Capital',
      phone: "+58 416 2106521",
      type: "Vendedor",
      task: [ { status: false ,task: 'Gestion de procesos'} ]
    },
    {
      location:    'Avenida Miranda, Caracas, Distrito Capital',
      phone: "+58 414 9461234",
      type: "Comerciante",
      task: [ { status: false ,task: "Enviar email a andrea" } ]
    },
    {
      location:    'C. C. La Tahona, Avenida Principal de la Tahona, Caracas, Miranda',
      phone: "+58 412 9876543",
      type: "Comerciante",
      task: [ { status: false ,task: 'Almuerso con gerente'} ]
    },
    {
      location:    'La California Norte, Caracas, Miranda',
      phone: "+58 416 2344987",
      type: "Comerciante",
      task: [ { status: false ,task: 'Cita odontologia'} ]
    },
    {
      location:    'Parque Carabobo, Caracas, Capital District',
      phone: "+58 416 1239031",
      type: "Comerciante",
      task: [ { status: false ,task: 'Reunion con recursos humanos'} ]
    },
    {
      location:    'Av. Casanova, Caracas 1052, Distrito Capital',
      phone: "+58 416 0842787",
      type: "Vendedor",
      task: [ { status: false ,task: 'Entrega de sprint'} ]
    },
    {
      location:    'Avenida Centro America, Caracas 1040, Distrito Capital',
      phone: "+58 416 9823123",
      type: "Comerciante",
      task: [ { status: false ,task: 'Facturacion de empresa'} ]
    },
    {
      location:    'Centro Comercial Expreso Baruta, Caracas 1080, 1080, Distrito Capital',
      phone: "+58 416 9893123",
      type: "Vendedor",
      task: [ { status: false ,task: 'Pago de nomina'} ]
    },
    {
      location:    'Residencias Paraiso, Avenida Las Fuentes, Caracas 1020, Distrito Capital',
      phone: "+58 416 8942982",
      type: "Comerciante",
      task: [ { status: false ,task: 'Buscar productos '} ]
    },
    {
      location:    'Calle El Cristo, Caracas 1030, Districto Federal',
      phone: "+58 416 3120862",
      type: "Comerciante",
      task: [ { status: false ,task: 'Entrega de proyecto final'} ]
    },
    {
      location:    'Avenida Este 2, Guarenas 1220, Miranda',
      phone: "+58 416 9871234",
      type: "Comerciante",
      task: [ { status: false ,task: 'Limpieza hogar'} ]
    },
    {
      location:    'C. C. La Tahona, Avenida Principal de la Tahona, Caracas, Miranda',
      phone: "+58 416 1230972",
      type: "Comerciante",
      task: [ { status: false ,task: "Review"} ]
    },
  ]

}
