import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/shared/user.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
    public page: number = 1;
    public indexEdit:any;
    public tab = true
    public loader:boolean = false
    public email = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    public create: boolean = false
    public user: any = {
      first_name: '',
      last_name: '',
      email: ''
    }
    public task = ''
  
    constructor ( 
      public router: Router,
      public userService: UserService,
      private route: ActivatedRoute,
      ){
      
      }
      
      ngOnInit() {
      this.indexEdit = this.route.snapshot.params.id
      this.getSelectedUser(this.indexEdit)
    }
  
    showAlert( type:any , title:any , message:any){
      Swal.fire(
        title,
        message,
        type,
        
      ).then((result:any) => {
        if (result.value) {
          this.create = false
          this.user = {
            first_name: '',
            last_name: '',
            email: ''
          }
        }
      })
    }

    addTask(){
      if(this.task != ''){
        this.userService.address[this.indexEdit - 1]['task'].push({status: false, task: this.task})
        this.task = ''

      }
    }

  
    valid(){
      if(this.user.first_name != '' && this.user.last_name != '' && this.user.email != '' && this.email.test(this.user.email) ){
        return false
      }else{
        return true
      }
    }

    updateUser(){
        this.loader = true
        this.userService.updateUser(this.user).subscribe(data =>{
          console.log(data)
          this.loader = false
          this.showAlert('success', 'success', 'Usuario editado exitosamente')
          
        }, error => {
          this.loader = false
          console.log(error)
          this.showAlert('error', 'error', 'error inesperado')
        })
    
    }

    change(){
      this.router.navigateByUrl('/');
    }
  
    getSelectedUser(id:number){
      this.loader = true
      this.userService.getSelectedUser(id).subscribe(data => {
        this.loader = false
        this.user = data.data
        console.log('data',data)
      }, error => {
        this.loader = false
        this.loader = false
        this.change()
        Swal.fire(
          'Usuario no encontrado', 'error', 'error', 
          
        )
      })
    }
}
