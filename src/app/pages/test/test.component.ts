import { Component, OnInit } from '@angular/core';
import { LogicService } from '../../shared/logic.service';
import { debounceTime } from 'rxjs/operators';
import Swal from 'sweetalert2'
import { Router } from '@angular/router';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  public loader:boolean = false
  public first_number:any = ''
  public tab = true
  public try = 0
  public second_number:any = ''
  public array_number:any[] = []

  constructor(public logicService: LogicService,  public router: Router, ) { }

  ngOnInit(): void {
  }

  suma(){

    if(this.loader == true){
      return
    }
    
    let small_number =  Number(this.first_number) <= Number(this.second_number) ? Number(this.first_number) : Number(this.second_number)
    let big_number =  Number(this.first_number) <= Number(this.second_number) ? Number(this.second_number) : Number(this.first_number)
    
    if(big_number - small_number >= 9999){
      
      Swal.fire(
        'La diferencia entre los numeros no puede ser mayor a 9999', 'error', 'error', 
        )
        return 
      }
      
    this.loader = true
    this.tab = false
    this.logicService.getRange(small_number, big_number).pipe(
      debounceTime(2000)).subscribe((data:any) => {
        this.try++
        this.array_number = data.square
        this.loader = false
        this.first_number = ''
        this.second_number = ''
      })
  }
}

