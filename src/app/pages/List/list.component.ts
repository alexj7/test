import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/shared/user.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

    public createdUsers:any = [];
    public users:any[] = [];
    public search: any = '';
    public page: number = 1;
    public indexEdit:any;
    public loader:boolean = false
    public email = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    public create: boolean = false
    public maxPage: any;
  
    constructor ( 
      public userService: UserService,   
      public router: Router,){
      
    }
  
    ngOnInit() {
      // this.showAlert('success', 'success', 'cargado plataforma')
      
      this.getUser(this.page)
    }
  
    extraPage(){
      if(this.createdUsers.length != 0){
        if(this.createdUsers.length < 6){
          return [3]
        }
        let qty =  Math.ceil(this.createdUsers.length / 6)
        let aux = []
        for (let index = 1; index <= Number(qty); index++) {
         aux.push(index + 2)
        }
        return aux
      }
      return
    }
  
    myUser(page:number){
      let first = 0
      for (let index = 3; index < page; index++) {
        first = first + 6
      }
      let users = this.createdUsers.slice(first, first + 6)
      if(users.length != 0){
        this.users = users
        this.page = page
      }
    }
  
    getUser(page:number){
      this.loader = true
      this.userService.getUser(page).subscribe(data => {
        this.loader = false
        this.users = data.data
        this.maxPage = data.total_pages
        console.log('data',data)
      }, error => {
        this.loader = false
          console.log(error)
      })
    }
  
    changePage(page:number){
      if(page > this.maxPage){
        page = this.maxPage
        return 
      }
      if(page < 1){
        page = 1
        return
      }
      this.page = page
      this.getUser(this.page)
    }

    editUser(id:number){
      const user_id = (id + 1) * this.page
      this.router.navigateByUrl(`/detail/${user_id}`);
    }
  
  
    delUser(id:number){
      if(this.page > 2){
        let aux;
        this.createdUsers.forEach((element:any, index:number) =>{
          element.id == id
          aux = index
        })
        this.createdUsers.splice(aux, 1)
        this.myUser(this.page)
        return
      }
  
      this.userService.delUser(id).subscribe(data => {
        this.users.splice(this.indexEdit, 1)
        if(this.indexEdit > 12){
          this.createdUsers = this.users
        }
        if(this.users.length == 0){
          this.getUser(this.page)
        }
        this.loader = false
      }, error => {
          this.loader = false
          Swal.fire(
            'error inesperado', 'error', 'error', 
            
          )
      })
    }
    
  

}
